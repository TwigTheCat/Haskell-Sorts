module Main
  ( main
  ) where

main :: IO ()
main = do
  -- print $ bblsrt [3, 2, 1, 5, 4, 1, 7]
  -- print $ qcksrt [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
  -- print $ mrgsrt [3, 45, 4356, 56, 78567, 86, 78, 46423, 5]
  print $ mrgsrt $ reverse [1..1000000]

-- bubble sort
bblsrt :: Ord a => [a] -> [a]
bblsrt xs
  | null xs = []
  | [head xs] == xs = xs
  | head xs <= xs !! 1 = head xs : bblsrt (tail xs)
  | otherwise = bblsrt (xs !! 1 : bblsrt (head xs : drop 2 xs))

-- quick sort
qcksrt :: Ord a => [a] -> [a]
qcksrt [] = []
qcksrt xs =
  (qcksrt . filter (< last xs)) xs ++
  [last xs] ++ (qcksrt . filter (> last xs)) xs

halve :: [a] -> ([a], [a])
halve xs = splitAt half xs
  where
    half = length xs `div` 2


mrgsrt :: Ord a => [a] -> [a]
mrgsrt [] = []
mrgsrt [x] = [x]
mrgsrt xs = merge (mrgsrt (fst halves)) (mrgsrt (snd halves))
  where
    halves = halve xs

merge :: Ord a => [a] -> [a] -> [a]
merge l1 [] = l1
merge [] l2 = l2
merge (x:xs) (y:ys)
  | x <= y = x : merge xs (y : ys)
  | otherwise = y : merge (x : xs) ys


genList :: [Int] -> Int -> [Int]

genList xs lim
  | null xs = [0]
  | last xs == lim = xs
  | otherwise = genList (xs++[last xs + 1]) lim
